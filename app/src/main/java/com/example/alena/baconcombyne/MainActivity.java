package com.example.alena.baconcombyne;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements ProfileFragment.OnFragmentInteractionListener, EditProfileFragment.OnFragmentInteractionListener, ProfilePictureFragment.OnProfilePictureInteractionListener, MainFragment.OnMainFragmentInteractionListener, LoginFragment.OnLoginFragmentInteractionListener, SignUpFragment.OnSignUpFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, MainFragment.newInstance("",""));
        fragmentTransaction.commit();
    }



    @Override
    public void openLoginFragment() {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, LoginFragment.newInstance("",""));
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();

    }

    @Override
    public void openSignUpFragment() {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, SignUpFragment.newInstance("",""));
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void openMeain() {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, MainFragment.newInstance("",""));
        fragmentTransaction.commit();
    }

    @Override
    public void openProfile() {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, ProfileFragment.newInstance("",""));
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    @Override
    public void onEditProfile() {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, EditProfileFragment.newInstance("", ""));
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();

    }

    @Override
    public void openProfilePictureFragment(String userEmail) {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, ProfilePictureFragment.newInstance(userEmail,""));
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    @Override
    public void onLogin(String userEmail) {
        FragmentManager fragment=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragment.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, ProfilePictureFragment.newInstance(userEmail,""));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
